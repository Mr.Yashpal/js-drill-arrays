export function filter(elements, Myfunction) {
    let newArray = [],count=0;
    for (let index = 0; index < elements.length; index++) {
        if(Myfunction(elements[index]) == true)
            newArray[count++] = elements[index];
    }
    return newArray;
}