export function find(elements, Myfunction) {
    for (let index = 0; index < elements.length; index++) {
        if(Myfunction(elements[index]) == true)
            return  elements[index];
    }
    return undefined;
}