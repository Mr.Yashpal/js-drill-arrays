let newItems = [];
export function map(elements, myFunction) {
    for (let index = 0; index < elements.length; index++) {
        newItems[index] = myFunction(elements[index]);
    }
    return newItems;
}