export function each(elements, myFunction) {
    for (let index = 0; index < elements.length; index++) {
        myFunction(elements[index],index);
    }
}