import { each } from '../each.js'
const elements = [1, 2, 3, 4, 5, 5];
let sum = 0;

function myFunction(value, index) {
    sum += value;
}

each(elements, myFunction);

const expectation = 20;

if (sum == expectation)
    console.log('Test Passed!');
else
    console.log('Test Failed!');