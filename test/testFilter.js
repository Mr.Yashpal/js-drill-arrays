import { filter } from '../filter.js'

const items = [1, 2, 3, 4, 5, 5];

let result = filter(items, checkAdult);

function checkAdult(age) {
  return age >= 18;
}

const expectation = [];

if (`${result}` == `${expectation}`)
  console.log('Test Passed!');
else
  console.log('Test Failed!');