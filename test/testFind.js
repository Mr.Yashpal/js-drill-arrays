import { find } from '../find.js'

const ages = [3, 10, 18, 20];

function checkAge(age) {
    return age > 18;
}

let result = find(ages, checkAge);

const expectation = 20;

if (result == expectation)
    console.log('Test Passed!');
else
    console.log('Test Failed!');