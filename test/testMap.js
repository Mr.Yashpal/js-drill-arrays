import { map } from '../map.js'
const items = [1, 2, 3, 4, 5, 5];

let newItems = map(items, myFunction);

function myFunction(num) {
  return num * 10;
}

const expectation = [10, 20, 30, 40, 50, 50];

if (`${newItems}` == `${expectation}`)
  console.log('Test Passed!');
else
  console.log('Test Failed!');