import { reduce } from '../reduce.js'

const items = [1, 2, 3, 4, 5, 5];

let result = reduce(items, getSum, 21);

function getSum(total, num) {
  return total + num;
}

const expectation = 41;

if (result == expectation)
  console.log('Test Passed!');
else
  console.log('Test Failed!');