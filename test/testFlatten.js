import { flatten } from '../flatten.js'

const nestedArray = [1, [2], [[3]], [[[4]]]];

let simpleArray = flatten(nestedArray);

const expectation = [1, 2, 3, 4];

if (`${simpleArray}` == `${expectation}`)
    console.log('Test Passed!');
else
    console.log('Test Failed!');